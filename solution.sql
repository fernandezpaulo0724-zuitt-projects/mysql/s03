CREATE DATABASE blog_db;

USE blog_db;

CREATE TABLE users(
	id INT NOT NULL AUTO_INCREMENT,
    email VARCHAR(100) NOT NULL,
    pw VARCHAR(300) NOT NULL,
    datetime_created DATETIME NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE posts(
	id INT NOT NULL AUTO_INCREMENT,
    user_id INT NOT NULL,
    title VARCHAR(500) NOT NULL,
    content VARCHAR(5000) NOT NULL,
    datetime_posted DATETIME NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT fk_posts_user_id
    	FOREIGN KEY (user_id) REFERENCES users(id)
    	ON UPDATE CASCADE
    	ON DELETE RESTRICT
);

CREATE TABLE post_comments(
	id INT NOT NULL AUTO_INCREMENT,
    post_id INT NOT NULL,
    user_id INT NOT NULL,
    content VARCHAR(5000) NOT NULL,
    datetime_commented DATETIME NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT fk_post_comments_post_id
    	FOREIGN KEY (post_id) REFERENCES posts(id)
    	ON UPDATE CASCADE
    	ON DELETE RESTRICT,
    CONSTRAINT fk_post_comments_user_id
    	FOREIGN KEY (user_id) REFERENCES users(id)
    	ON UPDATE CASCADE
    	ON DELETE RESTRICT
);

CREATE TABLE post_likes(
	id INT NOT NULL AUTO_INCREMENT,
    post_id INT NOT NULL,
    user_id INT NOT NULL,
    datetime_liked DATETIME NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT fk_post_likes_post_id
    	FOREIGN KEY (post_id) REFERENCES posts(id)
    	ON UPDATE CASCADE
    	ON DELETE RESTRICT,
    CONSTRAINT fk_post_likes_user_id
    	FOREIGN KEY (user_id) REFERENCES users(id)
    	ON UPDATE CASCADE
    	ON DELETE RESTRICT
);

INSERT INTO users(datetime_created, email, pw) VALUES ("2021-1-1 01:00:00", "johnsmith@gmail.com", "passwordA");
INSERT INTO users(datetime_created, email, pw) VALUES ("2021-1-1 02:00:00", "juandelacruz@gmail.com", "passwordB");
INSERT INTO users(datetime_created, email, pw) VALUES ("2021-1-1 03:00:00", "janesmith@gmail.com", "passwordC");
INSERT INTO users(datetime_created, email, pw) VALUES ("2021-1-1 04:00:00", "mariadelacruz@gmail.com", "passwordD");
INSERT INTO users(datetime_created, email, pw) VALUES ("2021-1-1 05:00:00", "johndoe@gmail.com", "passwordE");

INSERT INTO posts(content, datetime_posted, title, user_id) VALUES ("Hello World!", "2021-1-2 01:00:00", "First Code", 1);
INSERT INTO posts(content, datetime_posted, title, user_id) VALUES ("Hello Earth!", "2021-1-2 02:00:00", "Second Code", 1);
INSERT INTO posts(content, datetime_posted, title, user_id) VALUES ("Welcome to Mars!", "2021-1-2 03:00:00", "Third Code", 2);
INSERT INTO posts(content, datetime_posted, title, user_id) VALUES ("Bye bye solar system!", "2021-1-2 04:00:00", "Fourth Code", 4);

SELECT title FROM posts WHERE user_id=1;

SELECT email, datetime_created FROM users;

UPDATE posts SET content="Hello to the people of the Earth!" WHERE id=2;

DELETE FROM users WHERE email="johndoe@gmail.com";