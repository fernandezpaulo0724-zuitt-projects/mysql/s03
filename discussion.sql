-- Inserting Records
-- Command -> INSERT INTO tbl_name(column) VALUES ("col_value");
-- MySQL date format: year-month-day
-- Insert value into artists
INSERT INTO artists(name) VALUES ("Rivermaya");
INSERT INTO artists(name) VALUES ("Psy");

-- Insert value into albums
INSERT INTO albums(album_title, date_released, artist_id) VALUES ("Psy 6", "2012-1-1", 2);
INSERT INTO albums(album_title, date_released, artist_id) VALUES ("Trip", "1996-1-1", 1);

-- Insert values into songs
INSERT INTO songs(song_name, length, genre, album_id) VALUES ("Gangnam Style", 253, "K-Pop", 4)
INSERT INTO songs(song_name, length, genre, album_id) VALUES ("Kundiman", 234, "OPM", 2);
INSERT INTO songs(song_name, length, genre, album_id) VALUES ("Kisapmata", 279, "OPM", 2);

-- Selecting Records
-- Display the title and genre of all the songs
-- an asterisk (*) means it will return all
SELECT song_name, genre FROM songs;

-- Display all albums/songs
SELECT * FROM albums;
SELECT * FROM songs;

-- Display the title of all the OPM songs
SELECT song_name FROM songs WHERE genre = "OPM";

-- We can use AND and OR keyword for multiple expressions in the WHERE clause


-- Display the title and length of the OPM songs that are more than 2 minutes.
SELECT song_name, length FROM songs WHERE length > 200 AND genre = "OPM";

-- Display the title and length of a song specific to its name
SELECT song_name, length FROM songs WHERE song_name="Kundiman";

-- Updating the length of a song
UPDATE songs SET length = 250 WHERE song_name="Kisapmata";
UPDATE songs SET length = 240 WHERE song_name="Kundiman";

-- Deleting Records
-- Delete all OPM songs that are more than or equal to 2 minutes
DELETE FROM songs WHERE genre="OPM" AND length >= 200;

-- Delete all rows in songs table by removing the WHERE keyword
DELETE FROM songs;
